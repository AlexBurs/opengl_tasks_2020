﻿#version 330

uniform sampler2D diffuseTex;

struct LightInfo
{
	vec3 dir; //направление на источник света в мировой системе координат (для направленного источника)
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in mat4 viewMat;

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;

	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	
	vec4 lightDirCamSpace = viewMat * normalize(vec4(light.dir, 0.0)); //направление на источник света	

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);
        fragColor = vec4(color, 1.0); //просто копируем
}
