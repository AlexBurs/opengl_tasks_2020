#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <stdlib.h>
#include <algorithm>
#include <SOIL2.h>

class MapLoader {
public:
	std::vector<std::vector<std::vector<float>>> height_map;
	std::vector<std::vector<std::vector<float>>> color_map;
	int height;
	int width;

	void ReadMap() {
		const char* bump_path = "../../692BursikovData1/earthbump1k.jpg";
		const char* map_path = "../../692BursikovData1/earthmap1k.jpg";

		loadImg(height_map, bump_path, SOIL_LOAD_L);
		loadImg(color_map, map_path, SOIL_LOAD_RGB);

		assert(height_map.size() == color_map.size());
		assert(height_map.size() > 0);
		assert(height_map[0].size() == color_map[0].size());
		assert(height_map[0].size() > 0);
		assert(color_map[0][0].size() == 3);
		assert(height_map[0][0].size() == 1);

	    height = height_map.size();
		width = height_map[0].size();
	}

private:
	void loadImg(std::vector<std::vector<std::vector<float>>>& img, const char* path, int format) {
		int channels;
		int width;
		int height;

		unsigned char* src = SOIL_load_image
		(
			path,
			&width, &height, &channels,
			format
		);

		if (src == NULL) {
			std::cout << "NULL map, check file paths" << std::endl;
			throw std::runtime_error("error");
		}

		img = std::vector<std::vector<std::vector<float>>>(height, std::vector<std::vector<float>>(width, std::vector<float>(channels, 0.0)));
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				for (int k = 0; k < channels; ++k) {
					img[i][j][k] = static_cast<float>(*src) / 255;
					++src;
				}
			}
		}
	}
	
};

class SampleApplication : public Application
{
private :
	MapLoader map;

	MeshPtr _surface;
	ShaderProgramPtr _shader;

	std::vector<glm::vec3> _points;
	std::vector<glm::vec3> _colors;
	std::vector<glm::vec3> _normals;
public:
	SampleApplication() {
		const int k_neighbours = 6;

		int neighbours[k_neighbours][2] = {
			{ 0, 0 },
			{ 0, 1 },
			{ 1, 0 },
			{ 1, 0 },
			{ 0, 1 },
			{ 1, 1 },
		};

		map.ReadMap();

		const float h_coef = 0.008;
		const float norm_coef = 1 / float(map.width - 1);
		for (int i = 0; i < map.height - 1; ++i) {
			for (int j = 0; j < map.width - 1; ++j) {
				for (int k = 0; k < k_neighbours; ++k) {

					float i_ = i + neighbours[k][0];
					float j_ = j + neighbours[k][1];

					_points.push_back(glm::vec3(i_ * norm_coef, j_ * norm_coef,
						map.height_map[i_][j_][0] * h_coef));
					_colors.push_back(glm::vec3(map.color_map[i_][j_][0], map.color_map[i_][j_][1], map.color_map[i_][j_][2]));
				}
			}
		}

		transform();
		gen_normals();
	}

	void makeScene()
	{
		std::cerr << "makeScene" << std::endl;

		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();
		generate_mesh();

		_shader = std::make_shared<ShaderProgram>("../../692BursikovData1/shader.vert", "../../692BursikovData1/shader.frag");
		_shader->use();
	}

	void draw()
	{
		Application::draw();

		int width, height; 
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());

		_surface->draw();
	}

private:
	void gen_normals() {
		_normals.resize(_points.size());
		for (int i = 0; i < _points.size(); i += 3) {
			glm::vec3 a = _points[i] - _points[i + 1];
			glm::vec3 b = _points[i] - _points[i + 2];
			glm::vec3 normal = glm::normalize(glm::cross(a, b));
			_normals[i] = normal;
			_normals[i + 1] = normal;
			_normals[i + 2] = normal;
		}
	}

	void transform() {
		const float norm_coef = 1 / float(map.width);
		const float pi = 3.14159;
		for (int i = 0; i < _points.size(); ++i) {
			float a = _points[i][0];
			float b = _points[i][1];
			float c = _points[i][2];

			float angl1 = a / map.height / norm_coef * pi - pi / 2;

			float R = map.width * norm_coef / 2 / pi;
			float z = (R + c) * std::sin(angl1);
			float r = (R + c) * std::cos(angl1);
			float angl2 = b / map.width / norm_coef * 2 * pi;

			float x = r * std::sin(angl2);
			float y = r * std::cos(angl2);

			_points[i][0] = x;
			_points[i][1] = y;
			_points[i][2] = z;
		}
	}

	void generate_mesh() {
		DataBufferPtr buf_points = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf_points->setData(_points.size() * sizeof(float) * 3, _points.data());

		DataBufferPtr buf_norms = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf_norms->setData(_normals.size() * sizeof(float) * 3, _normals.data());

		DataBufferPtr buf_colors = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf_colors->setData(_colors.size() * sizeof(float) * 3, _colors.data());

		_surface = std::make_shared<Mesh>();
		_surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_points);
		_surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_norms);
		_surface->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_colors);

		_surface->setPrimitiveType(GL_TRIANGLES);
		_surface->setVertexCount(_points.size());

		const float scale = 10.f;
		glm::vec3 scalingVector = glm::vec3(scale, scale,scale);
		glm::tmat4x4<double> translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
		glm::tmat4x4<double> scalingMatrix = glm::scale(scalingVector);
		glm::tmat4x4<double> modelMatrix = translationMatrix * scalingMatrix;

		_surface->setModelMatrix(modelMatrix);
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}
